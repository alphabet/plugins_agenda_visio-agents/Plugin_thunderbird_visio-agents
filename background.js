/**
 * background.js
 * 
 * This file is part of the Thunderbird BigBlueButton extension
 * 
 * @author Thomas Payen <thomas.payen@apitech.fr>
 * 
 * @licence EUPL (https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)
 */

"use strict";

/* globals browser, messenger */

var init = async () => {
   // Register content and resources
   messenger.WindowListener.registerChromeUrl([
      ["content",    "tbbbb",    "chrome/content/tbbbb"],
      ["resource",   "tbbbb",    "chrome/", "contentaccessible=yes"],
   ]);

   // Inject the main script.
   messenger.WindowListener.registerWindow(
      "chrome://messenger/content/messenger.xhtml", 
      "chrome://tbbbb/content/tbbbb/loader-main-window.js");

   // Inject the script in event dialog.
   messenger.WindowListener.registerWindow(
      "chrome://calendar/content/calendar-event-dialog.xhtml", 
      "chrome://tbbbb/content/tbbbb/loader-event-window.js");

   // Start to listen for open window.
   messenger.WindowListener.startListening();
};

init();

console.info(
   browser.i18n.getMessage("extensionName") +
      " " +
      browser.runtime.getManifest().version
);