/**
 * options.js
 * 
 * This file is part of the Thunderbird BigBlueButton extension
 * Based on options.js from GCalendar module by Philipp Kewisch
 * 
 * @author Thomas Payen <thomas.payen@apitech.fr>
 * 
 * @licence EUPL (https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)
 */

/* globals browser, messenger */

function changeSetting(id, event) {
  messenger.storage.local.set({ ["settings." + id]: event.target.value });
}

for (let node of document.querySelectorAll("[data-l10n-id]")) {
  node.textContent = messenger.i18n.getMessage(node.getAttribute("data-l10n-id"));
}

for (let node of document.querySelectorAll("input")) {
  node.value = messenger.storage.local.get("settings." + node.id) ?? messenger.i18n.getMessage('settings.' + node.id);
}

(async function() {
  let prefs = await messenger.storage.local.get({
    "settings.eventAttendeeLink": messenger.i18n.getMessage('settings.eventAttendeeLink'),
    "settings.eventModeratorLink": messenger.i18n.getMessage('settings.eventModeratorLink'),
  });

  for (let [id, value] of Object.entries(prefs)) {
    let node = document.getElementById(id.substr(9));
    node.checked = value;
    node.addEventListener("change", changeSetting.bind(undefined, id));
  }
})();
