"use strict";

ChromeUtils.import("resource://gre/modules/Services.jsm");

/* globals  ExtensionParent, MozXULElement, Services, SessionStoreManager */

var TbBBBEvent = {
    DEBUG: true,
    TRACE: false,

    // TODO: Add a configuration for this
    URL: 'https://visio-test.education.fr/',
    API_URL: '/api/meetings',
    
    e(element) {
        return document.getElementById(element);
    },
    
    get addonName() {
        return "Thunderdird BBB";
    },
    
    get addonId() {
        return "Tb-BBB@apitech.fr";
    },
    
    get extensionInfo() {
        return ExtensionParent.GlobalManager.getExtension(this.addonId);
    },

    getBaseURL(relPath) {
        return this.extensionInfo.rootURI.resolve(relPath);
    },
    
    getWXAPI(extension, name, sync = false) {
        function implementation(api) {
            return api.getAPI({ extension })[name];
        }
        
        if (sync) {
            let api = extension.apiManager.getAPI(name, extension, "addon_parent");
            return implementation(api);
        }
        return extension.apiManager
            .asyncGetAPI(name, extension, "addon_parent")
            .then(api => {
                return implementation(api);
            });
    },
    
    /*
     * Strings.
     */
    getLocaleMessage(key, substitutions) {
        if (!this.i18n) {
            this.i18n = this.getWXAPI(this.extensionInfo, "i18n", true);
            // Init some strings.
            //this.extensionBye = this.i18n.getMessage("extensionBye");
        }
        
        return this.i18n.getMessage(key, substitutions);
    },
    
    onLoad() {
        this.DEBUG && console.debug("onLoad: --> START");
        
        this.CompleteInit();
        
        this.DEBUG && console.debug("onLoad: --> Tb-BBB-Event DONE");
    },
    
    onUnload() {
        this.DEBUG && console.debug("onUnload: --> Tb-BBB-Event ");

        this.RestoreOverlayElements();
        
        this.DEBUG && console.debug("onUnload: --> Tb-BBB-Event DONE");
    },
    
    async CompleteInit() {
        this.InitializeOverlayElements();
        
        this.DEBUG && console.debug("CompleteInit: --> Tb-BBB-Event");
    },
    
    InitializeOverlayElements() {
        this.DEBUG && console.debug("InitializeOverlayElements: ");

        this.InitializeStyleSheet(
            window.document,
            "chrome://tbbbb/content/tbbbb/tbbbb.css",
            this.addonName,
            true
        );
        
        // Add Event BBB button to calendar toolbar
        if (this.e('calendar-neweventbbb-button')) {
            this.e('calendar-neweventbbb-button').remove();
        }
        this.e("calendar-toolbar2").insertBefore(
            MozXULElement.parseXULToFragment(`
                <toolbarbutton id="calendar-neweventbbb-button" extension="${this.addonId}" 
                        class="toolbarbutton-1 calbar-toolbarbutton-1" 
                        label="${this.getLocaleMessage("newEventBBBLabel")}" 
                        tooltiptext="${this.getLocaleMessage("newEventBBBTooltip")}" 
                        type="menu"
                        removable="true">
                    <menupopup id="button-tbBBBPopup">
                        <menuseparator id="button-tbBBBSeparator"/>
                        <menuitem id="button-refreshBBBRooms" 
                                class="refreshBBBRooms" 
                                label="${this.getLocaleMessage("refreshRooms")}" 
                                oncommand="TbBBB.loadBBBRooms(true)"/>
                        <menuitem id="button-manageBBBRooms" 
                                class="manageBBBRooms" 
                                label="${this.getLocaleMessage("manageRooms")}" 
                                oncommand="TbBBB.manageBBBRooms()"/>
                    </menupopup>
                </toolbarbutton>
            `),
            this.e("calendar-newtask-button")
        );

        // Load rooms from api meetings
        this.loadBBBRooms(true);
            
        this.DEBUG && console.debug("InitializeOverlayElements: DONE");
    },

    /**
     * Destroy elements added by extension
     */
    RestoreOverlayElements() {
        let nodes = document.querySelectorAll(`[extension="${this.addonId}"]`);
        for (let node of nodes) {
          node.remove();
        }
        this.DEBUG && console.debug("RestoreOverlayElements: DONE");
    },

    /**
     * Inject a stylesheet, either chrome file or addon relative file.
     *
     * @param {Document} doc            - Document for the css injection.
     * @param {String} styleSheetSource - Resource or relative file name.
     * @param {String} styleName        - Name for DOM title.
     */
    InitializeStyleSheet(doc, styleSheetSource, styleName) {
        this.DEBUG && console.debug("InitializeStyleSheet: ");
        let href = styleSheetSource;

        this.TRACE &&
            console.debug("InitializeStyleSheet: styleSheet - " + styleSheetSource);
        this.TRACE && console.debug("InitializeStyleSheet: href - " + href);

        let link = doc.createElement("link");
        link.setAttribute("id", this.addonId);
        link.setAttribute("title", styleName);
        link.setAttribute("rel", "stylesheet");
        link.setAttribute("type", "text/css");
        link.setAttribute("href", href);
        // The |sheet| property is now (post Tb78) added after the sheet loads.
        // We must do this when setting title else another extension using this
        // technique may have its sheet be the |document.selectedStyleSheetSet|.
        link.setAttribute("onload", "this.sheet.disabled=false");
        doc.documentElement.appendChild(link);
        this.TRACE && console.debug(link.sheet);
    },

    /**
     * Add rooms to button-tbBBBPopup menu
     * 
     * @param {string} json 
     */
    addRoomsToMenu(json) {
        this.DEBUG && console.debug("addRoomsToMenu: --> Tb-BBB-Event ");

        // Clean old rooms
        let rooms = document.querySelectorAll('#button-tbBBBPopup > .room');
        for (let room of rooms) {
            room.remove();
        }

        // For each meeting add a attendee link and a moderator link
        if (json.meetings) {
            for (const room of json.meetings) {
                // Escape single quote
                let attendee_url = room.attendee_url.replace(/'/g, "\\'"),
                    moderator_url = room.moderator_url.replace(/'/g, "\\'"),
                    name = room.name.replace(/'/g, "\\'");

                this.e("button-tbBBBPopup").insertBefore(
                    MozXULElement.parseXULToFragment(`
                        <menu class="room" 
                                label="${room.name}" 
                                type="menu">
                            <menupopup>
                                <menuitem class="attendeeRoom" 
                                        label="${this.getLocaleMessage("newEventAttendeesLink")}" 
                                        oncommand="TbBBB.createBBBAttendeeEvent('${attendee_url}', '${name}')"/>
                                <menuitem class="moderatorRoom" 
                                        label="${this.getLocaleMessage("newEventModeratorsLink")}" 
                                        oncommand="TbBBB.createBBBModeratorEvent('${moderator_url}', '${name}')"/>
                            </menupopup>
                        </menu>
                    `),
                    this.e("button-tbBBBSeparator")
                );
            }
        }
    },

    /**
     * Call Calendar createEventWithDialog with url and description
     * 
     * @param {string} url 
     * @param {string} title 
     * @param {string} description 
     */
    createBBBEvent(url, title, description) {
        this.DEBUG && console.debug("createBBBEvent: --> Tb-BBB-Event ");

        let item = new CalEvent();

        // Initialize event with default values (such as calendar or dates)
        setDefaultItemValues(item);

        item.title = title;
        item.setProperty("DESCRIPTION", description);
        item.setProperty("URL", url);
        item.setProperty("LOCATION", url);

        // Call create event Dialog with item
        createEventWithDialog(null, null, null, null, item);
    },

    /**
     * Create a BBB event with attendee link
     * 
     * @param {string} url 
     * @param {string} name 
     */
    createBBBAttendeeEvent(url, name) {
        this.DEBUG && console.debug("createBBBAttendeeEvent: --> Tb-BBB-Event ");
        this.createBBBEvent(url, name, this.getLocaleMessage("eventAttendeeLink")
                .replace(/{link}/, url)
                .replace(/{name}/, name));
    },

    /**
     * Create a BBB event with moderator link
     * 
     * @param {string} url 
     * @param {string} name 
     */
     createBBBModeratorEvent(url, name) {
        this.DEBUG && console.debug("createBBBModeratorEvent: --> Tb-BBB-Event ");
        this.createBBBEvent(url, name, this.getLocaleMessage("eventModeratorLink")
                .replace(/{link}/, url)
                .replace(/{name}/, name));
    },

    /**
     * Load BBB rooms list from API meetings
     * if this.BOUCHON = true use fake data
     * 
     * @param {boolean} firstTry If it's not a first try, the login is probably wrong
     * @returns 
     */
    loadBBBRooms(firstTry) {
        this.DEBUG && console.debug("loadBBBRooms: --> Tb-BBB-Event ");

        // Keep this for async process
        var _this = this;

        // Fetch with credentials for session cookie
        fetch(new Request(this.URL + this.API_URL), { 
                method: 'GET',
                credentials: 'include',
            })
            .then(function(response) {
                if (response.status === 200) {
                    let contentType = response.headers.get("content-type");
                    if (contentType && contentType.indexOf("application/json") !== -1) {
                        return response.json().then(function(json) {
                            // This is the json we are looking for
                            _this.addRoomsToMenu(json);
                        });
                    }
                    else {
                        // Not a json, we probably need to log-in
                        return response.text().then(function(htmlString) {
                            // Parse HTML to find the form url and use it for authenticate
                            let doc = (new DOMParser()).parseFromString(htmlString, "text/html");
                            const url = doc.getElementById('kc-form-login').action;
                            _this.userAuthService(url, !firstTry);
                        });
                    }
                }
            });
    },

    /**
     * Open BBB frontend to manage rooms
     */
    manageBBBRooms() {
        this.DEBUG && console.debug("manageBBBRooms: --> Tb-BBB-Event ");

        let messenger = Cc["@mozilla.org/messenger;1"].createInstance();
        messenger = messenger.QueryInterface(Ci.nsIMessenger);
        messenger.launchExternalURL(this.URL);
    },

    /**
     * Send user authentication to SSO and retrieve credentials cookie
     * 
     * @param {string} url 
     * @param {boolean} force Force the prompt password and don't use saved password
     * @returns 
     */
    userAuthService(url, force) {
        this.DEBUG && console.debug("userAuthService: --> Tb-BBB-Event ");

        // Get use authentication informations from Password Manager
        let authInfo = this.getUserAuthInfo(url, force);

        // If the user cancel authentication
        if (!authInfo.username) {
            return;
        }

        // POST with username and password (form encoded)
        let init = { 
                method: 'POST',
                credentials: 'include',
                body: "username=" + authInfo.username + "&password=" + authInfo.password + "&credentials=",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
        };

        // Keep this for async process
        var _this = this;

        // Fetch with credentials for session cookie
        fetch(new Request(url), init)
            .then(function(response) {
                if (response.status === 200) {
                    let contentType = response.headers.get("content-type");
                    if (contentType && contentType.indexOf("application/json") !== -1) {
                        return response.json().then(function(json) {
                            // This is the json we are looking for
                            _this.TRACE && console.debug("userAuthService: --> json = " + JSON.stringify(json));
                            _this.addRoomsToMenu(json);
                        });
                    }
                    else {
                        // This is probably a failed user/password, just retry with force prompt
                        _this.userAuthService(url, true);
                    }
                }
            });
    },

    /**
     * Get user authentication information from prompt or saved password
     * 
     * @param {string} url 
     * @param {boolean} forcePrompt Force the prompt password and don't use saved password
     * @returns 
     */
    getUserAuthInfo(url, forcePrompt) {
        this.DEBUG && console.debug("getUserAuth: --> Tb-BBB-Event ");

        // Get shorter URL for password manager
        const pathArray = url.split( '/' );
        const shortUrl = pathArray[0] + '//' + pathArray[2];

        // If forcePrompt = false we can try to get the saved password
        if (!forcePrompt) {
            try {
                // Try to get existing login
                let existingLogins = Services.logins.findLogins(
                    shortUrl,
                    null,
                    this.getLocaleMessage("realm")
                );
                if (existingLogins.length) {
                    this.DEBUG && console.debug("getUserAuthInfo: --> Use saved password");
                    return existingLogins[0];
                }
            } catch (ex) {
                console.error("getUserAuthInfo: --> Exception: " + ex);
            }
        }

        // Informations for promptAuth
        let authInfo = {},
            savePassword = { value: true },
            channel = Services.io.newChannelFromURI(
                Services.io.newURI(url),
                null,
                Services.scriptSecurityManager.getSystemPrincipal(),
                null,
                Ci.nsILoadInfo.SEC_ALLOW_CROSS_ORIGIN_SEC_CONTEXT_IS_NULL,
                Ci.nsIContentPolicy.TYPE_OTHER
        );
        
        // Prompt authentication to the user
        Services.prompt.promptAuth(
            Services.wm.getMostRecentWindow(""),
            channel,
            1,
            authInfo,
            this.getLocaleMessage("savePasswordLabel"),
            savePassword
        );

        // Use password manager to save the user password
        if (authInfo.username && savePassword.value) {
            this.DEBUG && console.debug("getUserAuth: --> Save password");
            // write the data to the login manager.
            let loginInfo = new Components.Constructor(
                "@mozilla.org/login-manager/loginInfo;1",
                Ci.nsILoginInfo,
                "init"
            );
            let login = new loginInfo(
                shortUrl,
                null,
                this.getLocaleMessage("realm"),
                authInfo.username,
                authInfo.password,
                "",
                ""
            );
            try {
                let existingLogins = Services.logins.findLogins(
                    shortUrl,
                    null,
                    this.getLocaleMessage("realm")
                );
                if (existingLogins.length) {
                    Services.logins.modifyLogin(existingLogins[0], login);
                    this.DEBUG && console.debug("getUserAuth: --> Password added !");
                } else {
                    Services.logins.addLogin(login);
                    this.DEBUG && console.debug("getUserAuth: --> Password modified !");
                }
            } catch (ex) {
                console.error("getUserAuth: --> Exception: " + ex);
            }
        }
        return authInfo;
    }
}; // TbBBB
    
TbBBB.DEBUG &&
    console.debug(
        "TbBBB: readystate:session_restored - " +
        window.document.readyState +
        ":" +
        SessionStoreManager._restored
    );
    
(async function() {
    if (!["interactive", "complete"].includes(window.document.readyState)) {
        await new Promise(resolve =>
            window.addEventListener("load", resolve, { once: true })
        );
    }
        
    TbBBB.onLoad();
})();