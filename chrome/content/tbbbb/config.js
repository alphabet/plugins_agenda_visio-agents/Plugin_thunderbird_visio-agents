/**
 * config.js
 * 
 * This file is part of the Thunderbird BigBlueButton extension
 * 
 * @author Thomas Payen <thomas.payen@apitech.fr>
 * 
 * @licence EUPL (https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)
 */

"use strict";

var TbbbbConfig = {
    /**
     * URL to BigBlueButton frontend
     */
    BBB_URL: 'https://visio-test.education.fr/',

    /**
     * API meetings for BigBlueButton frontend
     */
    API_URL: 'api/meetings',

    /**
     * SSO URL for token
     */
    SSO_URL: 'https://sso-test.visio.education.fr/auth/realms/master/protocol/openid-connect/auth?client_id=visio-test&response_type=token&scope=openid+email+profile&redirect_uri=https%3A%2F%2Fvisio-test.education.fr%2Foidc_callback&state=SBobosHR9Wugs7eE&nonce=wybYwsxKytT3jWl3',
    
    /**
     * OICD callback url (used to detect auth from web)
     */
    OIDC_CALLBACK: 'https://visio-test.education.fr/oidc_callback',

    /**
     * Client ID for SSO (used only with password manager)
     */
    CLIENT_ID: '',

    /**
     * Client ID for SSO (used only with password manager if necessary)
     */
    CLIENT_SECRET: '',

    /**
     * Use password manager or web page for authentification
     */
    UsePasswordManager: false,

    /**
     * Log DEBUG in console
     */
    DEBUG: false,

    /**
     * Log TRACE in console
     */
    TRACE: false

}; // TbbbbUtils
