/**
 * loader-main-window.js
 * 
 * This file is part of the Thunderbird BigBlueButton extension
 * 
 * @author Thomas Payen <thomas.payen@apitech.fr>
 * 
 * @licence EUPL (https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)
 */

// Import any needed modules.
var { Services } = ChromeUtils.import("resource://gre/modules/Services.jsm");

// Load an additional JavaScript file.
Services.scriptloader.loadSubScript("chrome://tbbbb/content/tbbbb/config.js", window, "UTF-8");

// Load an additional JavaScript file.
Services.scriptloader.loadSubScript("chrome://tbbbb/content/tbbbb/utils.js", window, "UTF-8");

// Load an additional JavaScript file.
Services.scriptloader.loadSubScript("chrome://tbbbb/content/tbbbb/main-window.js", window, "UTF-8");

function onLoad(activatedWhileWindowOpen) {

}

function onUnload(deactivatedWhileWindowOpen) {
  // Cleaning up the window UI is only needed when the
  // add-on is being deactivated/removed while the window
  // is still open. It can be skipped otherwise.
  if (!deactivatedWhileWindowOpen) {
    return
  }
}
