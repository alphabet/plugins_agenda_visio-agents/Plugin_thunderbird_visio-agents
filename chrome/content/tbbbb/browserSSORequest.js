/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

var { MailE10SUtils } = ChromeUtils.import("resource:///modules/MailE10SUtils.jsm");

var wpl = Ci.nsIWebProgressListener;

// Suppresses an error from LoginManagerPrompter where PopupNotifications is not defined. Taking it
// from the main window.
window.PopupNotifications = window.opener?.PopupNotifications;

window.reportUserClosed = function() {
  let request = window.arguments[0].request;
  request.cancelled();
};

window.loadRequestedUrl = function() {
  let request = window.arguments[0].request;

  let browser = document.getElementById("requestFrame");

  let url = request.url;
  if (url != "") {
    MailE10SUtils.loadURI(browser, url);
  }

  request.loaded(window, browser.webProgress);
};
