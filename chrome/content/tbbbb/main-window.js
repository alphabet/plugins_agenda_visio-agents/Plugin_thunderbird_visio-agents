/**
 * main-window.js
 * 
 * This file is part of the Thunderbird BigBlueButton extension
 * 
 * @author Thomas Payen <thomas.payen@apitech.fr>
 * 
 * @licence EUPL (https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)
 */

"use strict";

var { Services } = ChromeUtils.import("resource://gre/modules/Services.jsm");

/* globals  ExtensionParent, MozXULElement, Services, SessionStoreManager */

var TbBBB = {
    
    onLoad() {
        TbbbbConfig.DEBUG && console.debug("onLoad: --> START");
        
        this.CompleteInit();
        
        TbbbbConfig.DEBUG && console.debug("onLoad: --> Tb-BBB DONE");
    },
    
    onUnload() {
        TbbbbConfig.DEBUG && console.debug("onUnload: --> Tb-BBB ");

        this.RestoreOverlayElements();
        
        TbbbbConfig.DEBUG && console.debug("onUnload: --> Tb-BBB DONE");
    },
    
    async CompleteInit() {
        this.InitializeOverlayElements();
        
        TbbbbConfig.DEBUG && console.debug("CompleteInit: --> Tb-BBB");
    },
    
    InitializeOverlayElements() {
        TbbbbConfig.DEBUG && console.debug("InitializeOverlayElements: ");

        this.InitializeStyleSheet(
            window.document,
            "chrome://tbbbb/content/tbbbb/tbbbb.css",
            TbbbbUtils.addonName,
            true
        );
        
        // Add Event BBB button to calendar toolbar
        if (TbbbbUtils.e('calendar-neweventbbb-button')) {
            TbbbbUtils.e('calendar-neweventbbb-button').remove();
        }

        // Is object found
        if (TbbbbUtils.e("calendar-toolbar2")) {
            TbbbbUtils.e("calendar-toolbar2").insertBefore(
                MozXULElement.parseXULToFragment(`
                    <toolbarbutton id="calendar-neweventbbb-button" extension="${TbbbbUtils.addonId}" 
                            class="toolbarbutton-1 calbar-toolbarbutton-1" 
                            label="${TbbbbUtils.getLocaleMessage("newEventBBBLabel")}" 
                            tooltiptext="${TbbbbUtils.getLocaleMessage("newEventBBBTooltip")}" 
                            type="menu"
                            removable="true">
                        <menupopup id="button-tbBBBPopup">
                            <menuseparator id="button-tbBBBSeparator"/>
                            <menuitem id="button-refreshBBBRooms" 
                                    class="refreshBBBRooms" 
                                    label="${TbbbbUtils.getLocaleMessage("refreshRooms")}" 
                                    oncommand="TbbbbUtils.loadBBBRooms()"/>
                            <menuitem id="button-manageBBBRooms" 
                                    class="manageBBBRooms" 
                                    label="${TbbbbUtils.getLocaleMessage("manageRooms")}" 
                                    oncommand="TbbbbUtils.manageBBBRooms()"/>
                        </menupopup>
                    </toolbarbutton>
                `),
                TbbbbUtils.e("calendar-newtask-button")
            );
        }
        
        // Load rooms from api meetings
        TbbbbUtils.loadBBBRooms(true);
            
        TbbbbConfig.DEBUG && console.debug("InitializeOverlayElements: DONE");
    },

    /**
     * Destroy elements added by extension
     */
    RestoreOverlayElements() {
        let nodes = document.querySelectorAll(`[extension="${TbbbbUtils.addonId}"]`);
        for (let node of nodes) {
          node.remove();
        }
        TbbbbConfig.DEBUG && console.debug("RestoreOverlayElements: DONE");
    },

    /**
     * Inject a stylesheet, either chrome file or addon relative file.
     *
     * @param {Document} doc            - Document for the css injection.
     * @param {String} styleSheetSource - Resource or relative file name.
     * @param {String} styleName        - Name for DOM title.
     */
    InitializeStyleSheet(doc, styleSheetSource, styleName) {
        TbbbbConfig.DEBUG && console.debug("InitializeStyleSheet: ");
        let href = styleSheetSource;

        TbbbbConfig.TRACE &&
            console.debug("InitializeStyleSheet: styleSheet - " + styleSheetSource);
        TbbbbConfig.TRACE && console.debug("InitializeStyleSheet: href - " + href);

        let link = doc.createElement("link");
        link.setAttribute("id", TbbbbUtils.addonId);
        link.setAttribute("title", styleName);
        link.setAttribute("rel", "stylesheet");
        link.setAttribute("type", "text/css");
        link.setAttribute("href", href);
        // The |sheet| property is now (post Tb78) added after the sheet loads.
        // We must do this when setting title else another extension using this
        // technique may have its sheet be the |document.selectedStyleSheetSet|.
        link.setAttribute("onload", "this.sheet.disabled=false");
        doc.documentElement.appendChild(link);
        TbbbbConfig.TRACE && console.debug(link.sheet);
    },

    /**
     * Add rooms to button-tbBBBPopup menu
     * 
     * @param {string} json 
     */
    addRoomsToMenu(json) {
        TbbbbConfig.DEBUG && console.debug("addRoomsToMenu: --> Tb-BBB ");

        // Clean old rooms
        let rooms = document.querySelectorAll('#button-tbBBBPopup > .room');
        for (let room of rooms) {
            room.remove();
        }

        // For each meeting add a attendee link and a moderator link
        if (json.meetings) {
            for (const room of json.meetings) {
                // Escape single quote
                let attendee_url = room.attendee_url.replace(/'/g, "\\'"),
                    moderator_url = room.moderator_url.replace(/'/g, "\\'"),
                    name = room.name.replace(/'/g, "\\'");

                TbbbbUtils.e("button-tbBBBPopup").insertBefore(
                    MozXULElement.parseXULToFragment(`
                        <menu class="room" 
                                label="${room.name}" 
                                type="menu">
                            <menupopup>
                                <menuitem class="attendeeRoom" 
                                        label="${TbbbbUtils.getLocaleMessage("newEventAttendeesLink")}" 
                                        oncommand="TbbbbUtils.createBBBAttendeeEvent('${attendee_url}', '${name}')"/>
                                <menuitem class="moderatorRoom" 
                                        label="${TbbbbUtils.getLocaleMessage("newEventModeratorsLink")}" 
                                        oncommand="TbbbbUtils.createBBBModeratorEvent('${moderator_url}', '${name}')"/>
                            </menupopup>
                        </menu>
                    `),
                    TbbbbUtils.e("button-tbBBBSeparator")
                );
            }
        }
    }
}; // TbBBB

TbbbbConfig.DEBUG &&
    console.debug(
        "TbBBB: readystate:session_restored - " +
        window.document.readyState +
        ":" +
        SessionStoreManager._restored
    );

(async function() {
    if (!["interactive", "complete"].includes(window.document.readyState)) {
        await new Promise(resolve =>
            window.addEventListener("load", resolve, { once: true })
        );
    }
        
    TbBBB.onLoad();
})();