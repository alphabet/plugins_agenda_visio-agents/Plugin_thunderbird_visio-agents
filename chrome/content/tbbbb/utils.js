/**
 * utils.js
 * 
 * This file is part of the Thunderbird BigBlueButton extension
 * 
 * @author Thomas Payen <thomas.payen@apitech.fr>
 * 
 * @licence EUPL (https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)
 */

"use strict";

var { Services } = ChromeUtils.import("resource://gre/modules/Services.jsm");
var { ExtensionParent } = ChromeUtils.import("resource://gre/modules/ExtensionParent.jsm");

/* globals  ExtensionParent, MozXULElement, Services */

var TbbbbUtils = {
    _token: undefined,
    _403errors: 0,
        
    e(element) {
        return document.getElementById(element);
    },
    
    get addonName() {
        return "Thunderdird BBB";
    },
    
    get addonId() {
        return "tb-bbb@apitech.fr";
    },
    
    get extensionInfo() {
        return ExtensionParent.GlobalManager.getExtension(this.addonId);
    },

    getBaseURL(relPath) {
        return this.extensionInfo.rootURI.resolve(relPath);
    },
    
    getWXAPI(extension, name, sync = false) {
        function implementation(api) {
            return api.getAPI({ extension })[name];
        }
        
        if (sync) {
            let api = extension.apiManager.getAPI(name, extension, "addon_parent");
            return implementation(api);
        }
        return extension.apiManager
            .asyncGetAPI(name, extension, "addon_parent")
            .then(api => {
                return implementation(api);
            });
    },
    
    /*
     * Strings.
     */
    getLocaleMessage(key, substitutions) {
        if (!this._i18n) {
            this._i18n = this.getWXAPI(this.extensionInfo, "i18n", true);
            // Init some strings.
            // this.requestWindowTitle = this._i18n.getMessage("requestWindowTitle");
        }
        
        return this._i18n.getMessage(key, substitutions);
    },

    /*
     * Storage local
     */
    getLocalStorage() {
        if (!this._storage) {
            function getStorage(localstorage) {
                try {
                  localstorage.local.get = (...args) =>
                    localstorage.local.callMethodInParentProcess("get", args);
                  localstorage.local.set = (...args) =>
                    localstorage.local.callMethodInParentProcess("set", args);
                  localstorage.local.remove = (...args) =>
                    localstorage.local.callMethodInParentProcess("remove", args);
                  localstorage.local.clear = (...args) =>
                    localstorage.local.callMethodInParentProcess("clear", args);
                } catch (e) {
                  console.info("Storage permission is missing");
                }
                return localstorage;
            }
            this._storage = getStorage(this.getWXAPI(this.extensionInfo, "storage", true));
        }
        
        return this._storage.local;
    },
    
    /**
     * Call Calendar createEventWithDialog with url and description
     * 
     * @param {string} url 
     * @param {string} title 
     * @param {string} description 
     */
    createBBBEvent(url, title, description) {
        TbbbbConfig.DEBUG && console.debug("createBBBEvent: --> Tb-BBB ");

        let item = new CalEvent();

        // Initialize event with default values (such as calendar or dates)
        setDefaultItemValues(item);

        item.title = title;
        item.setProperty("DESCRIPTION", description);
        // Don't use URL for now
        // item.setProperty("URL", url);
        item.setProperty("LOCATION", url);

        // Call create event Dialog with item
        createEventWithDialog(null, null, null, null, item);
    },

    /**
     * Replace event location and description in calendar-event-dialog
     * 
     * @param {string} url 
     * @param {string} title 
     * @param {string} description_text 
     * @param {string} description_html 
     */
    showBBBEvent(url, title, description_text, description_html) {
        TbbbbConfig.DEBUG && console.debug("showBBBEvent: --> Tb-BBB ");

        let iframeWindow = this.e('calendar-item-panel-iframe').contentWindow;

        // Replace location by new url
        iframeWindow.document.getElementById("item-location").value = url;

        // Description
        let editorElement = iframeWindow.document.getElementById("item-description");
        let editor = editorElement.getHTMLEditor(editorElement.contentWindow);

        editor.insertHTML(description_html + '<br><br>');
    },

    /**
     * Create a BBB event with attendee link
     * 
     * @param {string} url 
     * @param {string} name 
     */
    createBBBAttendeeEvent(url, name) {
        TbbbbConfig.DEBUG && console.debug("createBBBAttendeeEvent: --> Tb-BBB ");
        this.createBBBEvent(url, name, this.getLocaleMessage("eventAttendeeLink")
                .replace(/{link}/g, url)
                .replace(/{name}/g, name));
    },

    /**
     * Change a BBB event with attendee link
     * 
     * @param {string} url 
     * @param {string} name 
     */
    showBBBAttendeeEvent(url, name) {
        TbbbbConfig.DEBUG && console.debug("showBBBAttendeeEvent: --> Tb-BBB ");
        let description_text = this.getLocaleMessage("eventAttendeeLink")
            .replace(/{link}/g, url)
            .replace(/{name}/g, name);
        let description_html = this.getLocaleMessage("eventAttendeeLinkHTML")
            .replace(/{link}/g, url)
            .replace(/{name}/g, name);
        this.showBBBEvent(url, name, description_text, description_html);
    },

    /**
     * Create a BBB event with moderator link
     * 
     * @param {string} url 
     * @param {string} name 
     */
    createBBBModeratorEvent(url, name) {
        TbbbbConfig.DEBUG && console.debug("createBBBModeratorEvent: --> Tb-BBB ");
        this.createBBBEvent(url, name, this.getLocaleMessage("eventModeratorLink")
                .replace(/{link}/g, url)
                .replace(/{name}/g, name));
    },

    /**
     * Create a BBB event with moderator link
     * 
     * @param {string} url 
     * @param {string} name 
     */
    showBBBModeratorEvent(url, name) {
        TbbbbConfig.DEBUG && console.debug("showBBBModeratorEvent: --> Tb-BBB ");
        let description_text = this.getLocaleMessage("eventModeratorLink")
            .replace(/{link}/g, url)
            .replace(/{name}/g, name);
        let description_html = this.getLocaleMessage("eventModeratorLinkHTML")
            .replace(/{link}/g, url)
            .replace(/{name}/g, name);
        this.showBBBEvent(url, name, description_text, description_html);
    },

    /**
     * Load BBB rooms list from API meetings
     * 
     * @param {boolean} first First time calling this method
     * 
     * @returns 
     */
    loadBBBRooms(first = false) {
        TbbbbConfig.DEBUG && console.debug("loadBBBRooms: --> Tb-BBB ");

        // Keep this for async process
        var _this = this;

        let setRoomsFromStorage = result => {
            if (first && result['tbbbb.meetings.json']) {
                // This is the json we are looking for
                TbBBB.addRoomsToMenu(result['tbbbb.meetings.json']);
            }
            else {
                if (_this._token !== undefined) {
                    // Fetch with credentials for session cookie
                    fetch(new Request(TbbbbConfig.BBB_URL + TbbbbConfig.API_URL), { 
                        method: 'GET',
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "Authorization": "Bearer " + _this._token,
                        },
                    })
                    .then(function(response) {
                        if (response.status === 200) {
                            _this._403errors = 0;
                            let contentType = response.headers.get("content-type");
                            if (contentType && contentType.indexOf("application/json") !== -1) {
                                return response.json().then(function(json) {
                                    // This is the json we are looking for
                                    TbBBB.addRoomsToMenu(json);
        
                                    // Add the json to local storage
                                    _this.getLocalStorage().set({ "tbbbb.meetings.json": json });
                                });
                            }
                        }
                        else if (response.status === 401 || response.status === 403) {
                            // 401 or 403 we need to log-in with token
                            _this._403errors++;
                            // Manage errors loop
                            if (_this._403errors >= 2) {
                                alert(_this.getLocaleMessage("GetRoomsError"));
                            }
                            else {
                                _this.userAuthService();
                            }
                        }
                        else {
                            _this._403errors = 0;
                            alert(_this.getLocaleMessage("GetRoomsError"));
                            console.warn('Can\'t get rooms, unsupported status, response status : ' + response.status);
                        }
                    });
                }
                else {
                    // No token, need to auth
                    _this.userAuthService();
                }
            }
        };

        let onError = error => {
            console.error(`TbBBB.getRoomsFromStorage: ${error}`);
        };

        let getting = this.getLocalStorage().get("tbbbb.meetings.json");
        getting.then(setRoomsFromStorage, onError); 
    },

    /**
     * Open BBB frontend to manage rooms
     */
    manageBBBRooms() {
        TbbbbConfig.DEBUG && console.debug("manageBBBRooms: --> Tb-BBB ");

        this._browserRequest = {
            account: window,
            object: this,
            url: TbbbbConfig.BBB_URL,
            _active: true,
            cancelled: function() {
              if (!this._active) {
                return;
              }      
            },
      
            loaded: function(aWindow, aWebProgress) {
              if (!this._active) {
                return;
              }
      
              aWindow.document.title = this.object.getLocaleMessage("requestWindowTitleManage");
            },
        };

        window.request = this._browserRequest;

        // Open request window
        let parent = Services.wm.getMostRecentWindow("mail:3pane");
        Services.ww.openWindow(
            parent, 
            "chrome://tbbbb/content/tbbbb/browserSSORequest.xhtml",
            null, 
            "chrome,private,centerscreen,width=980,height=750", 
            window
        );
    },

    /**
     * Send user authentication to SSO and retrieve credentials cookie
     * 
     * @param {boolean} force Force the prompt password and don't use saved password
     * @returns 
     */
    userAuthService(force = false) {
        TbbbbConfig.DEBUG && console.debug("userAuthService: --> Tb-BBB ");

        // Use password manager or web authentification ?
        if (TbbbbConfig.UsePasswordManager) {
            // Get use authentication informations from Password Manager
            let authInfo = this.getUserAuthInfo(TbbbbConfig.SSO_URL, force);

            // If the user cancel authentication
            if (!authInfo.username) {
                return;
            }

            // POST with username and password (form encoded)
            let init = { 
                method: 'POST',
                body: "username=" + authInfo.username + "&password=" + authInfo.password + "&grant_type=password&client_id=" + TbbbbConfig.CLIENT_ID + "&client_secret=" + TbbbbConfig.CLIENT_SECRET,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
            };

            // Keep this for async process
            var _this = this;

            // Fetch with credentials for session cookie
            fetch(new Request(TbbbbConfig.SSO_URL), init)
                .then(function(response) {
                    if (response.status === 200) {
                        let contentType = response.headers.get("content-type");
                        if (contentType && contentType.indexOf("application/json") !== -1) {
                            return response.json().then(function(json) {
                                if (json.error) {
                                    console.warn('AuthError: ' + json.error_description);

                                    // This is certainly a failed user/password, just retry with force prompt
                                    _this.userAuthService(true);
                                }
                                else {
                                    // This is the json we are looking for
                                    _this.TRACE && console.debug("userAuthService: --> json = " + JSON.stringify(json));

                                    // Keep the token
                                    _this._token = json.access_token;

                                    // Call the load for the rooms
                                    _this.loadBBBRooms();
                                }
                            });
                        }
                        else {
                            console.warn('AuthError: Unexpected result');

                            // This is probably a failed user/password, just retry with force prompt
                            _this.userAuthService(true);
                        }
                    }
                    else {
                        console.warn('AuthError: response status ' + response.status);

                        // This is probably a failed user/password, just retry with force prompt
                        _this.userAuthService(true);
                    }
                });
        }
        else {
            this._browserRequest = {
                object: this,
                url: TbbbbConfig.SSO_URL,
                _active: true,
                cancelled: function() {
                  if (!this._active) {
                    return;
                  }      
                },
          
                loaded: function(aWindow, aWebProgress) {
                  if (!this._active) {
                    return;
                  }
          
                  this._listener = {
                    window: aWindow,
                    webProgress: aWebProgress,
                    _parent_object: this.object,
          
                    QueryInterface: ChromeUtils.generateQI([
                      "nsIWebProgressListener",
                      "nsISupportsWeakReference",
                    ]),
          
                    _cleanUp: function() {
                      this.webProgress.removeProgressListener(this);
                      this.window.close();
                      delete this.window;
                    },
          
                    onStateChange: function(aChangedWebProgress, aRequest, aStateFlags, aStatus) {},
                    onLocationChange: function(aChangedWebProgress, aRequest, aLocation) {   
                        if (aLocation.spec.indexOf(TbbbbConfig.OIDC_CALLBACK) === 0) {
                            let token = '';
                            const _params = aLocation.spec.split('#', 2).pop().split('&');
                            for (const key in _params) {
                                if (Object.hasOwnProperty.call(_params, key)) {
                                    const element = _params[key];
                                    if (element.indexOf('access_token=') === 0) {
                                        token = element.substring(13);
                                    }
                                }
                            }

                            // Cleanup the window
                            this._cleanUp();

                            if (token != '') {
                                // Keep the token
                                this._parent_object._token = token;

                                // Call the load for the rooms
                                this._parent_object.loadBBBRooms();
                            }
                            else {
                                // Error
                                alert(_this.getLocaleMessage("GetRoomsError"));
                            }
                        }
                    },
                    onProgressChange: function() {},
                    onStatusChange: function() {},
                    onSecurityChange: function() {},
                  };
                  aWebProgress.addProgressListener(this._listener, Ci.nsIWebProgress.NOTIFY_ALL);
                  aWindow.document.title = this.object.getLocaleMessage("requestWindowTitle");
                },
            };
    
            window.request = this._browserRequest;
    
            // Open request window
            let parent = Services.wm.getMostRecentWindow("mail:3pane");
            Services.ww.openWindow(
                parent, 
                "chrome://tbbbb/content/tbbbb/browserSSORequest.xhtml",
                null, 
                "chrome,private,centerscreen,width=980,height=750", 
                window
            );
        }        
    },

    /**
     * Get user authentication information from prompt or saved password
     * 
     * @param {string} url 
     * @param {boolean} forcePrompt Force the prompt password and don't use saved password
     * @returns 
     */
    getUserAuthInfo(url, forcePrompt) {
        TbbbbConfig.DEBUG && console.debug("getUserAuth: --> Tb-BBB ");

        // Get shorter URL for password manager
        const pathArray = url.split( '/' );
        const shortUrl = pathArray[0] + '//' + pathArray[2];

        // If forcePrompt = false we can try to get the saved password
        if (!forcePrompt) {
            try {
                // Try to get existing login
                let existingLogins = Services.logins.findLogins(
                    shortUrl,
                    null,
                    this.getLocaleMessage("realm")
                );
                if (existingLogins.length) {
                    TbbbbConfig.DEBUG && console.debug("getUserAuthInfo: --> Use saved password");
                    return existingLogins[0];
                }
            } catch (ex) {
                console.error("getUserAuthInfo: --> Exception: " + ex);
            }
        }

        // Informations for promptAuth
        let authInfo = {},
            savePassword = { value: true },
            channel = Services.io.newChannelFromURI(
                Services.io.newURI(url),
                null,
                Services.scriptSecurityManager.getSystemPrincipal(),
                null,
                Ci.nsILoadInfo.SEC_ALLOW_CROSS_ORIGIN_SEC_CONTEXT_IS_NULL,
                Ci.nsIContentPolicy.TYPE_OTHER
        );
        
        // Prompt authentication to the user
        Services.prompt.promptAuth(
            Services.wm.getMostRecentWindow(""),
            channel,
            1,
            authInfo,
            this.getLocaleMessage("savePasswordLabel"),
            savePassword
        );

        // Use password manager to save the user password
        if (authInfo.username && savePassword.value) {
            TbbbbConfig.DEBUG && console.debug("getUserAuth: --> Save password");
            // write the data to the login manager.
            let loginInfo = new Components.Constructor(
                "@mozilla.org/login-manager/loginInfo;1",
                Ci.nsILoginInfo,
                "init"
            );
            let login = new loginInfo(
                shortUrl,
                null,
                this.getLocaleMessage("realm"),
                authInfo.username,
                authInfo.password,
                "",
                ""
            );
            try {
                let existingLogins = Services.logins.findLogins(
                    shortUrl,
                    null,
                    this.getLocaleMessage("realm")
                );
                if (existingLogins.length) {
                    Services.logins.modifyLogin(existingLogins[0], login);
                    TbbbbConfig.DEBUG && console.debug("getUserAuth: --> Password added !");
                } else {
                    Services.logins.addLogin(login);
                    TbbbbConfig.DEBUG && console.debug("getUserAuth: --> Password modified !");
                }
            } catch (ex) {
                console.error("getUserAuth: --> Exception: " + ex);
            }
        }
        return authInfo;
    }
}; // TbbbbUtils
